<?php

/**
 * @file
 * Multiple_email module file.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\multiple_email\EmailInterface;

/**
 * Implements hook_help().
 */
function multiple_email_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.multiple_email':
      $path_resolver = \Drupal::service('extension.path.resolver');
      $file_system = \Drupal::service('file_system');

      $file_path = $path_resolver->getPath('module', 'multiple_email') . '/README.txt';
      $file = $file_system->realpath($file_path);

      return check_markup(file_get_contents($file));
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function multiple_email_user_insert(EntityInterface $entity) {
  /** @var \Drupal\user\UserInterface $entity */

  /** @var \Drupal\multiple_email\EmailStorageInterface $email_storage */
  $email_storage = \Drupal::entityTypeManager()->getStorage('multiple_email');

  $email = $email_storage->create([
    'uid' => $entity->id(),
    'email' => $entity->getEmail(),
    'time_registered' => $entity->getCreatedTime(),
    'status' => EmailInterface::CONFIRMED,
  ]);
  $email->save();
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function multiple_email_user_delete(EntityInterface $entity) {
  /** @var \Drupal\user\UserInterface $entity */

  /** @var \Drupal\multiple_email\EmailStorageInterface $email_storage */
  $email_storage = \Drupal::entityTypeManager()->getStorage('multiple_email');

  $email = $email_storage->loadByEmail($entity->getEmail());
  $email->delete();
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Adds a custom validator to the user registration form to check if the email
 * address in not already in use.
 */
function multiple_email_form_user_register_form_alter(&$form, FormStateInterface $form_state) {
  $form['account']['mail']['#element_validate'][] = '_multiple_email_user_register_form_validate';
}

/**
 * Custom validator for the user registration form.
 *
 * Check if the typed mail is already in use by another user.
 */
function _multiple_email_user_register_form_validate($element, FormStateInterface $form_state) {
  $mail = $element['#value'];

  if (!empty($mail)) {
    /** @var \Drupal\multiple_email\EmailStorageInterface $email_storage */
    $email_storage = \Drupal::entityTypeManager()->getStorage('multiple_email');
    $email = $email_storage->loadByEmail($mail);

    if ($email) {
      $message = t('The e-mail address %email is already registered.', [
        '%email' => $mail,
      ]);
      $form_state->setError($element, $message);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Remove e-mail field from profile edit for privileged users.
 * This will be done in address management screen instead.
 */
function multiple_email_form_user_form_alter(&$form, FormStateInterface $form_state) {
  // Don't do anything when creating a new account.
  if ($form['#id'] == 'user-register-form') {
    return;
  }

  $config = \Drupal::config('multiple_email.settings');
  $hide_field = $config->get('hide_field');
  $access = \Drupal::currentUser()->hasPermission('use multiple emails');

  if ($access && $hide_field) {
    $user = \Drupal::routeMatch()->getParameter('user');
    $link = Link::createFromRoute('Email addresses', 'multiple_email.manage', [
      'user' => $user->id(),
    ]);
    $description = t('Email addresses are managed on the @multiple_email tab.', [
      '@multiple_email' => $link->toString(),
    ]);

    $form['account']['mail']['#disabled'] = TRUE;
    $form['account']['mail']['#required'] = FALSE;
    $form['account']['mail']['#description'] = $description;
  }
}

/**
 * Implements hook_cron().
 */
function multiple_email_cron() {
  $config = \Drupal::config('multiple_email.settings');
  $confirm = $config->get('confirm');

  if ($confirm['deadline']) {
    /** @var \Drupal\multiple_email\EmailConfirmerInterface $email_confirmer */
    $email_confirmer = \Drupal::service('multiple_email.confirmer');

    /** @var \Drupal\multiple_email\EmailStorageInterface $email_storage */
    $email_storage = \Drupal::entityTypeManager()->getStorage('multiple_email');

    $deadline_time = strtotime('-' . $confirm['deadline'] . ' days');
    $query = $email_storage->getQuery()
      ->condition('status', EmailInterface::UNCONFIRMED)
      ->condition('time_code_generated', $deadline_time, '<');
    $ids = $query->execute();
    $emails = $email_storage->loadMultiple($ids);

    foreach ($emails as $email) {
      $email_confirmer->expire($email);
    }
  }
}

/**
 * Implements hook_token_info_alter().
 */
function multiple_email_token_info_alter(&$data) {
  $data['tokens']['multiple_email']['email'] = [
    'name' => t('Email'),
    'description' => t('The email address.'),
  ];

  $data['tokens']['multiple_email']['confirm_code'] = [
    'name' => t('Confirmation code'),
    'description' => t('The code to confirm the email address.'),
  ];

  $data['tokens']['multiple_email']['confirm_deadline'] = [
    'name' => t('Confirmation deadline'),
    'description' => t('The deadline for confirming the email address.'),
  ];

  $data['tokens']['multiple_email']['confirm_url'] = [
    'name' => t('Confirmation URL'),
    'description' => t('The URL to confirm the email address.'),
  ];
}

/**
 * Implements hook_tokens().
 */
function multiple_email_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'multiple_email' && !empty($data['multiple_email'])) {
    /** @var \Drupal\multiple_email\EmailInterface $email */
    $email = $data['multiple_email'];

    $config = \Drupal::config('multiple_email.settings');

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'email':
          $replacements[$original] = $email->getEmail();
          break;

        case 'confirm_code':
          $replacements[$original] = $email->getConfirmationCode();
          break;

        case 'confirm_deadline':
          $confirm = $config->get('confirm');

          if ($confirm['deadline']) {
            $days = \Drupal::translation()->formatPlural($confirm['deadline'], '1 day', '@count days');
          }
          else {
            $days = 'unlimited days';
          }

          $replacements[$original] = $days;
          break;

        case 'confirm_url':
          $desintation = Url::fromRoute('multiple_email.confirm_form', [
            'user' => $email->getOwnerId(),
            'multiple_email' => $email->id(),
            'code' => $email->getConfirmationCode(),
          ]);

          $url = Url::fromRoute('user.login')
            ->setOption('query', ['destination' => $desintation->toString()])
            ->setAbsolute();

          $replacements[$original] = $url->toString();
          break;
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_mail().
 */
function multiple_email_mail($key, &$message, $params) {
  switch ($key) {
    case 'confirmation':
    case 'expiration':
      $message['from'] = \Drupal::config('system.site')->get('mail');
      $message['subject'] = $params['subject'];
      $message['body'][] = $params['body'];
      break;
  }
}
