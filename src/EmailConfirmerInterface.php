<?php

namespace Drupal\multiple_email;

/**
 * Interface for defining the email confirmer service.
 */
interface EmailConfirmerInterface {

  /**
   * Confim an email address.
   *
   * @param \Drupal\multiple_email\EmailInterface $email
   *   The email address to confirm.
   */
  public function confirm(EmailInterface $email);

  /**
   * Cancel a confirmation attempt.
   *
   * @param \Drupal\multiple_email\EmailInterface $email
   *   The email address of the confirmation attempt to cancel.
   */
  public function cancelConfirmation(EmailInterface $email);

  /**
   * Expire an email address.
   *
   * @param \Drupal\multiple_email\EmailInterface $email
   *   The email address of the confirmation attempt to expire.
   */
  public function expire(EmailInterface $email);

}
