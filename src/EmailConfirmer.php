<?php

namespace Drupal\multiple_email;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;

/**
 * The email confirmer service.
 */
class EmailConfirmer implements EmailConfirmerInterface {
  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The datetime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The token service.
   *
   * @var Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs the service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TimeInterface $time, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager, Token $token, LoggerChannelInterface $logger) {
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->token = $token;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function confirm(EmailInterface $email) {
    $email
      ->setConfirmationCode($this->generateCode())
      ->setCodeGeneratedTime($this->time->getRequestTime())
      ->setAttempts(0)
      ->setStatus(EmailInterface::UNCONFIRMED)
      ->save();

    $this->send($email, 'confirmation');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelConfirmation(EmailInterface $email) {
    $email
      ->setStatus(EmailInterface::CANCELLED)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function expire(EmailInterface $email) {
    $email->delete();

    $this->send($email, 'expiration');
  }

  /**
   * Generates a random string of given length from given characters.
   *
   * If no characters are specified, then it uses a-zA-Z0-9. Characters are
   * specified as a string containing every valid character. Duplicates will
   * (in theory) increase that character's chances of occurring in the random
   * string.
   *
   * @param int $length
   *   Length of the random code. Defaults to 10 characters.
   * @param string $chars
   *   The characters to use in the code.
   *
   * @return string
   *   The generated code.
   */
  protected function generateCode($length = 10, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUBWXYZ0123456789') {
    $cl = mb_strlen($chars) - 1;
    $out = '';
    for ($i = 0; $i < $length; $i++) {
      $out .= $chars[mt_rand(0, $cl)];
    }

    return $out;
  }

  /**
   * Sends a message to the user.
   *
   * @param \Drupal\multiple_email\EmailInterface $email
   *   The email address to send the message to.
   * @param string $type
   *   The type of email to send. Can be either confirmation, or expiration.
   */
  protected function send(EmailInterface $email, $type) {
    $config = $this->configFactory->get('multiple_email.mail');
    $settings = $config->get($type);

    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    $variables = [
      'user' => $email->getOwner(),
      'multiple_email' => $email,
    ];
    $token_options = [
      'langcode' => $langcode,
      'clear' => TRUE,
    ];

    $to = $email->getEmail();
    $subject = $this->token->replace($settings['subject'], $variables, $token_options);
    $body = $this->token->replace($settings['body'], $variables, $token_options);

    $params = [
      'subject' => $subject,
      'body' => $body,
    ];

    $result = $this->mailManager->mail('multiple_email', 'confirmation', $to, $langcode, $params);
    if ($result['result']) {
      $log_message = $this->t('@type message sent to @email.', [
        '@type' => ucfirst($type),
        '@email' => $to,
      ]);
      $this->logger->notice($log_message);
    }
    else {
      $log_message = $this->t('Problem sending @type message to @email.', [
        '@type' => $type,
        '@email' => $to,
      ]);
      $this->logger->error($log_message);
    }
  }

}
