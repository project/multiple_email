<?php

namespace Drupal\multiple_email;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for email entities.
 */
class EmailStorage extends SqlContentEntityStorage implements EmailStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByUser(AccountInterface $account) {
    $properties = ['uid' => $account->id()];
    return $this->loadByProperties($properties);
  }

  /**
   * {@inheritdoc}
   */
  public function loadByEmail($email) {
    $properties = ['email' => $email];
    $email = $this->loadByProperties($properties);
    return reset($email);
  }

}
