<?php

namespace Drupal\multiple_email\Traits;

use Drupal\multiple_email\EmailConfirmerInterface;

/**
 * Functionality for the email confirmer service.
 */
trait EmailConfirmerTrait {

  /**
   * The email confirmer service.
   *
   * @var \Drupal\multiple_email\EmailConfirmerInterface
   */
  protected $emailConfirmer;

  /**
   * Set the email confirmer service.
   *
   * @param \Drupal\multiple_email\EmailConfirmerInterface $email_confirmer
   *   The email email confirmer service.
   *
   * @return $this
   */
  protected function setEmailConfirmer(EmailConfirmerInterface $email_confirmer) {
    $this->emailConfirmer = $email_confirmer;
    return $this;
  }

}
