<?php

namespace Drupal\multiple_email\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\multiple_email\EmailInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the email entity.
 *
 * @ContentEntityType(
 *   id = "multiple_email",
 *   label = @Translation("Email address"),
 *   label_collection = @Translation("Email addresses"),
 *   label_singular = @Translation("Email address"),
 *   label_plural = @Translation("Email addresses"),
 *   label_count = @PluralTranslation(
 *     singular = "@count email address",
 *     plural = "@count email addresses",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\multiple_email\EmailStorage",
 *   },
 *   base_table = "multiple_email",
 *   admin_permission = "administer multiple emails",
 *   entity_keys = {
 *     "id" = "eid",
 *     "label" = "email",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 * )
 */
class Email extends ContentEntityBase implements EmailInterface {
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('email')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($email) {
    $this->set('email', $email);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegisteredTime() {
    return $this->get('time_registered')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmationCode() {
    return $this->get('confirm_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfirmationCode($code) {
    $this->set('confirm_code', $code);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeGeneratedTime() {
    return $this->get('time_code_generated')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCodeGeneratedTime($generated) {
    $this->set('time_code_generated', $generated);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttempts() {
    return (int) $this->get('attempts')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAttempts($attempts) {
    $this->set('attempts', $attempts);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function incrementAttempts() {
    $attempts = $this->getAttempts() + 1;
    $this->set('attempts', $attempts);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isUnconfirmed() {
    return $this->get('status')->value != EmailInterface::CONFIRMED;
  }

  /**
   * {@inheritdoc}
   */
  public function isPending() {
    return !$this->isConfirmed() && !$this->isCancelled();
  }

  /**
   * {@inheritdoc}
   */
  public function isCancelled() {
    return $this->get('status')->value == EmailInterface::CANCELLED;
  }

  /**
   * {@inheritdoc}
   */
  public function isConfirmed() {
    return $this->get('status')->value == EmailInterface::CONFIRMED;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(new TranslatableMarkup('Email'))
      ->setRequired(TRUE)
      ->addConstraint('UniqueField');

    $fields['time_registered'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Registered'));

    $fields['confirm_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Confirmation Code'));

    $fields['time_code_generated'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Code generated'));

    $fields['attempts'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Attempts'));

    $fields['status'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Status'))
      ->setDefaultValue(EmailInterface::UNCONFIRMED);

    return $fields;
  }

}
