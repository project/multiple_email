<?php

namespace Drupal\multiple_email;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Interface for defining email entities.
 */
interface EmailInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * The email address is unconfirmed.
   */
  const UNCONFIRMED = 0;

  /**
   * The email address is confirmed.
   */
  const CONFIRMED = 1;

  /**
   * The email address confirmation is cancelled.
   */
  const CANCELLED = 2;

  /**
   * Returns the email address.
   *
   * @return string
   *   The email address.
   */
  public function getEmail();

  /**
   * Sets the email address.
   *
   * @param string $email
   *   The email address.
   *
   * @return $this
   */
  public function setEmail($email);

  /**
   * Returns the time the email address was registered.
   *
   * @return int
   *   The time the email address was registered.
   */
  public function getRegisteredTime();

  /**
   * Returns the confirmation code.
   *
   * @return string
   *   The confirmation code.
   */
  public function getConfirmationCode();

  /**
   * Sets the confirmation code.
   *
   * @param string $code
   *   The confirmation code.
   *
   * @return $this
   */
  public function setConfirmationCode($code);

  /**
   * Returns the time the confirmation code was generated.
   *
   * @return int
   *   The time the confirmation code was generated.
   */
  public function getCodeGeneratedTime();

  /**
   * Sets the time the confirmation code was generated.
   *
   * @param int $generated
   *   The time the confirmation code was generated.
   *
   * @return $this
   */
  public function setCodeGeneratedTime($generated);

  /**
   * Returns the number of attempts to confirm the email address.
   *
   * @return int
   *   The number of attempts to confirm the email address.
   */
  public function getAttempts();

  /**
   * Sets number of attempts to confirm the email address.
   *
   * @param int $attempts
   *   The number of attempts to confirm the email address.
   *
   * @return $this
   */
  public function setAttempts($attempts);

  /**
   * Increment the number of attempts to confirm the email address.
   *
   * @return $this
   */
  public function incrementAttempts();

  /**
   * Returns the status of the email address.
   *
   * @return int
   *   The status of the email address.
   */
  public function getStatus();

  /**
   * Sets the status of the email address.
   *
   * @param int $status
   *   The status of the email address.
   *
   * @return $this
   */
  public function setStatus($status);

  /**
   * Returns TRUE if the email address is unconfirmed.
   *
   * @return bool
   *   TRUE if the email address is unconfirmed, FALSE otherwise.
   */
  public function isUnconfirmed();

  /**
   * Returns TRUE if the email address confirmation is pending.
   *
   * @return bool
   *   TRUE if the email address confirmation is pending, FALSE otherwise.
   */
  public function isPending();

  /**
   * Returns TRUE if the email address confirmation is cancelled.
   *
   * @return bool
   *   TRUE if the email address confirmation is cancelled, FALSE otherwise.
   */
  public function isCancelled();

  /**
   * Returns TRUE if the email address is confirmed.
   *
   * @return bool
   *   TRUE if the email address is confirmed, FALSE otherwise.
   */
  public function isConfirmed();

}
