<?php

namespace Drupal\multiple_email\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\multiple_email\EmailInterface;
use Drupal\user\UserInterface;

/**
 * Checks access for Multiple Email actions.
 */
class MultipleEmailAccess implements AccessInterface {

  /**
   * Basic permission check for accessing pages.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   * @param \Drupal\user\UserInterface $user
   *   The user that the email entity is attached to.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function pageAccess(AccountInterface $account, UserInterface $user) {
    if ($account->hasPermission('administer multiple emails')) {
      return AccessResult::allowed();
    }

    if ($account->hasPermission('use multiple emails')) {
      if ($account->id() == $user->id()) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * Basic permission check for AJAX routes.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   * @param \Drupal\multiple_email\EmailInterface $multiple_email
   *   The email entity being modified.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function ajaxAccess(AccountInterface $account, EmailInterface $multiple_email) {
    if ($account->hasPermission('administer multiple emails')) {
      return AccessResult::allowed();
    }

    if ($account->hasPermission('use multiple emails')) {
      if ($account->id() == $multiple_email->getOwnerId()) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

}
