<?php

namespace Drupal\multiple_email\Form;

/**
 * Form for setting an email address as the primary email.
 */
class ConfirmSetPrimaryForm extends AjaxConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_confirm_set_primary';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDescription() {
    return $this->t('Are you sure you want to set %email as the primary email?', [
      '%email' => $this->email->getEmail(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function doAction() {
    $email_address = $this->email->getEmail();

    $user = $this->email->getOwner();
    $user->setEmail($email_address);
    $user->save();
  }

}
