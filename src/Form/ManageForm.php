<?php

namespace Drupal\multiple_email\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\multiple_email\EmailStorageInterface;
use Drupal\multiple_email\Traits\EmailConfirmerTrait;
use Drupal\multiple_email\Util\EmailOperations;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for managing an accounts email addresses.
 */
class ManageForm extends FormBase {
  use EmailConfirmerTrait;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The email storage.
   *
   * @var \Drupal\multiple_email\EmailStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = (new static())
      ->setEmailConfirmer($container->get('multiple_email.confirmer'))
      ->setFormBuilder($container->get('form_builder'))
      ->setStorage($container
        ->get('entity_type.manager')
        ->getStorage('multiple_email'));

    return $instance;
  }

  /**
   * Set the form builder service.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   *
   * @return $this
   */
  protected function setFormBuilder(FormBuilderInterface $form_builder) {
    $this->formBuilder = $form_builder;
    return $this;
  }

  /**
   * Set the email storage.
   *
   * @param \Drupal\multiple_email\EmailStorageInterface $storage
   *   The email storage.
   *
   * @return $this
   */
  protected function setStorage(EmailStorageInterface $storage) {
    $this->storage = $storage;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_manage';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $header = [
      'email' => $this->t('Email'),
      'status' => $this->t('Status'),
      'operations' => $this->t('Operations'),
    ];

    $form['emails'] = [
      '#type' => 'table',
      '#header' => $header,
      '#attributes' => [
        'id' => 'multiple-email-table-wrapper',
      ],
    ];

    $emails = $this->storage->loadByUser($user);
    foreach ($emails as $email) {
      $primary = $email->getEmail() == $user->mail->value;
      $operations = new EmailOperations($email);

      if ($email->isConfirmed()) {
        if ($primary) {
          $status_text = $this->t('Primary');
        }
        else {
          $status_text = $this->t('Confirmed');
          $operations->addSetPrimary();
        }
      }
      elseif ($email->isPending()) {
        $status_text = $this->t('Confirmation pending');
        $operations->addResend();
        $operations->addCancel();
      }
      elseif ($email->isCancelled()) {
        $status_text = $this->t('Confirmation cancelled');
        $operations->addResend();
      }
      else {
        $status_text = $this->t('Unconfirmed');
        $operations->addConfirm();
      }

      // The primary email address can't be removed.
      if (!$primary) {
        $operations->addRemove();
      }

      $form['emails'][$email->id()] = [
        'email' => [
          '#type' => 'markup',
          '#markup' => $email->getEmail(),
        ],
        'status' => [
          '#type' => 'markup',
          '#markup' => $status_text,
        ],
        'operations' => [
          '#type' => 'operations',
          '#links' => $operations->getAll(),
        ],
      ];
    }

    $form['emails']['add'] = [
      'email' => [
        '#type' => 'email',
        '#placeholder' => $this->t('Add a new email address'),
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Add email address'),
      ],
      'operations' => [],
    ];

    $action_url = Url::fromRoute('multiple_email.manage', [
      'user' => $user->id(),
    ]);

    $form['#action'] = $action_url->toString();
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue(['emails', 'add']);
    $email_address = $values['email'];

    $existing = $this->storage->loadByEmail($email_address);

    if ($existing) {
      $message = $this->t('Email address @email has already been registered.', [
        '@email' => $email_address,
      ]);
      $form_state->setErrorByName('email', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $emails = $form_state->getValue('emails');
    $email = reset($emails);
    $email_address = $email['email'];

    /** @var \Drupal\multiple_email\EmailInterface $email */
    $email = $this->storage->create([
      'email' => $email_address,
    ]);

    $email->save();
    $this->emailConfirmer->confirm($email);

    $message = $this->t('Email address @email added.', ['@email' => $email_address]);
    $this->messenger()->addMessage($message);

    $form_state->setRedirect('multiple_email.manage', [
      'user' => $email->getOwnerId(),
    ]);
  }

}
