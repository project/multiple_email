<?php

namespace Drupal\multiple_email\Form;

/**
 * Form for removing an email address.
 */
class ConfirmDeleteForm extends AjaxConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_confirm_delete';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDescription() {
    return $this->t('Are you sure you want to delete %email?', [
      '%email' => $this->email->getEmail(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function doAction() {
    $this->email->delete();
  }

}
