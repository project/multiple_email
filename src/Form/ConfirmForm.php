<?php

namespace Drupal\multiple_email\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multiple_email\EmailInterface;
use Drupal\multiple_email\Traits\EmailConfirmerTrait;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for managing an accounts e-mail addresses.
 */
class ConfirmForm extends FormBase {
  use EmailConfirmerTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = (new static())
      ->setEmailConfirmer($container->get('multiple_email.confirmer'));

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL, EmailInterface $multiple_email = NULL, $code = NULL) {
    $form['#email'] = $multiple_email;

    $message = $this->t('The e-mail address %email is awaiting confirmation. You
      should have received an e-mail at that address with a confirmation code in
      it. Enter the code below and click confirm.', [
        '%email' => $multiple_email->getEmail(),
      ]);

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $message . '</p>',
    ];

    $form['code'] = [
      '#type' => 'textfield',
      '#title' => t('Confirmation Code'),
      '#required' => TRUE,
      '#default_value' => Html::escape($code),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('multiple_email.settings');
    $confirmation_settings = $settings->get('confirm');

    /** @var \Drupal\multiple_email\EmailInterface $email */
    $email = $form['#email'];
    $attempts = $email->getAttempts();
    $code = $form_state->getValue('code');

    if ($attempts < $confirmation_settings['attempts']) {
      if ($code != $email->getConfirmationCode()) {
        $email
          ->setAttempts($attempts + 1)
          ->save();

        $form_state->setErrorByName('code', $this->t('The confirmation code was
          incorrect'));
      }
    }
    else {
      $this->emailConfirmer->confirm($email);

      $form_state->setErrorByName('', $this->t('You have exhausted your allowed
        attempts at confirming this e-mail address. A new confirmation code has
        been sent.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\multiple_email\EmailInterface $email */
    $email = $form['#email'];
    $email
      ->setStatus(EmailInterface::CONFIRMED)
      ->save();

    $message = $this->t('The address @email has been confirmed.', [
      '@email' => $email->getEmail(),
    ]);
    $this->messenger()->addStatus($message);

    $form_state->setRedirect('multiple_email.manage', [
      'user' => $email->getOwnerId(),
    ]);
  }

}
