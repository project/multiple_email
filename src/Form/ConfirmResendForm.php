<?php

namespace Drupal\multiple_email\Form;

/**
 * Form for resending an email address confirmation.
 */
class ConfirmResendForm extends AjaxConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_confirm_resend';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDescription() {
    return $this->t('Are you sure you want to resend the confirmation of %email?', [
      '%email' => $this->email->getEmail(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function doAction() {
    $this->emailConfirmer->confirm($this->email);
  }

}
