<?php

namespace Drupal\multiple_email\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for the admin settings page.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var self $instance */
    $instance = parent::create($container);
    $instance->setModuleHander($container->get('module_handler'));

    return $instance;
  }

  /**
   * Set the module handler service.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  protected function setModuleHander(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['multiple_email.settings', 'multiple_email.mail'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config_settings = $this->config('multiple_email.settings');
    $config_mail = $this->config('multiple_email.mail');
    $token_help = $this->t('Available variables are: [site:name], [site:url], [site:login-url], [user:display-name], [user:account-name], [multiple_email:email], [multiple_email:confirm_code], [multiple_email:confirm_url], [multiple_email:confirm_deadline].');

    $token_enabled = $this->moduleHandler->moduleExists('token');
    if ($token_enabled) {
      $token_help = $this->t('The list of available tokens that can be used in emails is provided below.');
    }

    $form['hide_field'] = [
      '#type' => 'select',
      '#title' => t('Hide E-mail Field'),
      '#description' => t('Hides the e-mail field when editing a user'),
      '#options' => ['No', 'Yes'],
      '#default_value' => $config_settings->get('hide_field'),
    ];

    $form['edit_emails'] = [
      '#type' => 'select',
      '#title' => t('Allow editing of emails'),
      '#description' => t('Allows editing of e-mail addresses. It is equivalent to deleting and adding a new e-mail address, as edited emails must be re-confirmed. If enabled, e-mail addresses (excluding primary) may be edited via the multiple e-mail tab.'),
      '#options' => ['No', 'Yes'],
      '#default_value' => $config_settings->get('edit_emails'),
    ];

    $form['confirm_attempts'] = [
      '#type' => 'textfield',
      '#size' => 4,
      '#title' => t('Confirm Attempts'),
      '#description' => t('How many times a user enters a confirmation code before a new one is generated. If set to 0, no new codes are sent after the first one.'),
      '#default_value' => $config_settings->get('confirm.attempts'),
    ];

    $form['confirm_deadline'] = [
      '#type' => 'textfield',
      '#size' => 4,
      '#title' => t('Confirm Days'),
      '#description' => t('How many days a user has to enter a confirmation code. If 0, emails pending confirmation do not expire.'),
      '#default_value' => $config_settings->get('confirm.deadline'),
    ];

    $form['email'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Emails'),
    ];

    $form['email_confirmation'] = [
      '#type' => 'details',
      '#title' => $this->t('Confirmation'),
      '#description' => $this->t('Customize the message to be sent when a user adds a new email to their account.') . ' ' . $token_help,
      '#group' => 'email',
    ];

    $form['email_confirmation']['confirmation_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config_mail->get('confirmation.subject'),
    ];

    $form['email_confirmation']['confirmation_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config_mail->get('confirmation.body'),
    ];

    $form['email_expiration'] = [
      '#type' => 'details',
      '#title' => $this->t('Expiration'),
      '#description' => $this->t('Customize the message to be sent when an unconfirmed email address expires.') . ' ' . $token_help,
      '#group' => 'email',
    ];

    $form['email_expiration']['expiration_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config_mail->get('expiration.subject'),
    ];

    $form['email_expiration']['expiration_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config_mail->get('expiration.body'),
    ];

    if ($token_enabled) {
      $form['email']['token_tree'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['user', 'multiple_email'],
        '#show_restricted' => TRUE,
        '#show_nested' => FALSE,
        '#weight' => 90,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('confirm_attempts'))) {
      $form_state->setErrorByName('confirm_attempts', $this->t('Confirm attempts must be an number!'));
    }
    if (!is_numeric($form_state->getValue('confirm_deadline'))) {
      $form_state->setErrorByName('confirm_deadline', $this->t('Confirm Days must be an number!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('multiple_email.settings')
      ->set('hide_field', (bool) $form_state->getValue('hide_field'))
      ->set('edit_emails', (bool) $form_state->getValue('edit_emails'))
      ->set('confirm.attempts', (int) $form_state->getValue('confirm_attempts'))
      ->set('confirm.deadline', (int) $form_state->getValue('confirm_deadline'))
      ->save();

    $this->config('multiple_email.mail')
      ->set('confirmation.subject', $form_state->getValue('confirmation_subject'))
      ->set('confirmation.body', $form_state->getValue('confirmation_body'))
      ->set('expiration.subject', $form_state->getValue('expiration_subject'))
      ->set('expiration.body', $form_state->getValue('expiration_body'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
