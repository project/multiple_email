<?php

namespace Drupal\multiple_email\Form;

/**
 * Form for cancelling an email address confirmation.
 */
class ConfirmCancelForm extends AjaxConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_confirm_cancel';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDescription() {
    return $this->t('Are you sure you want to cancel the confirmation of %email?', [
      '%email' => $this->email->getEmail(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function doAction() {
    $this->emailConfirmer->cancelConfirmation($this->email);
  }

}
