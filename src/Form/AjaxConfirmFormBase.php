<?php

namespace Drupal\multiple_email\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multiple_email\EmailInterface;
use Drupal\multiple_email\Traits\EmailConfirmerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for AJAX confirm forms.
 */
abstract class AjaxConfirmFormBase extends FormBase {
  use EmailConfirmerTrait;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The email address.
   *
   * @var \Drupal\multiple_email\EmailInterface
   */
  protected $email;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = (new static())
      ->setFormBuilder($container->get('form_builder'))
      ->setEmailConfirmer($container->get('multiple_email.confirmer'));

    return $instance;
  }

  /**
   * Set the form builder service.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   *
   * @return $this
   */
  protected function setFormBuilder(FormBuilderInterface $form_builder) {
    $this->formBuilder = $form_builder;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EmailInterface $multiple_email = NULL) {
    $this->email = $multiple_email;

    $form['description'] = [
      '#markup' => $this->getDescription(),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'button',
        '#button_type' => 'primary',
        '#value' => $this->t('Confirm'),
        '#ajax' => [
          'callback' => [$this, 'ajaxSubmit'],
        ],
      ],
      'cancel' => [
        '#type' => 'button',
        '#value' => $this->t('Cancel'),
        '#ajax' => [
          'callback' => [$this, 'closeDialog'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->doAction();

    $form_state->setRedirect('multiple_email.manage', [
      'user' => $this->email->getOwnerId(),
    ]);
  }

  /**
   * AJAX form submit handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    $this->doAction();

    $form = $this->formBuilder->getForm(ManageForm::class, $this->email->getOwner());

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    $response->addCommand(new ReplaceCommand('#multiple-email-table-wrapper', $form));

    $form_state->setResponse($response);

    return $response;
  }

  /**
   * Close the modal dialog.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function closeDialog() {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());

    return $response;
  }

  /**
   * Get the description of the action this form is confirming.
   */
  abstract protected function getDescription();

  /**
   * Perform the action this form is confirming.
   */
  abstract protected function doAction();

}
