<?php

namespace Drupal\multiple_email\Form;

/**
 * Form for confirming an email address.
 */
class ConfirmConfirmForm extends AjaxConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_email_confirm_confirm';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDescription() {
    return $this->t('Are you sure you want to confirm %email?', [
      '%email' => $this->email->getEmail(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function doAction() {
    $this->emailConfirmer->confirm($this->email);
  }

}
