<?php

namespace Drupal\multiple_email\Util;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\multiple_email\EmailInterface;

/**
 * Utility class to set operation links for an email entity.
 */
class EmailOperations {
  use StringTranslationTrait;

  /**
   * The email emtity to perform the operation on.
   *
   * @var \Drupal\multiple_email\EmailInterface
   */
  protected $email;

  /**
   * An array of operation links.
   *
   * @var array
   */
  protected $operations = [];

  /**
   * Construct the class.
   *
   * @param \Drupal\multiple_email\EmailInterface $email
   *   The email emtity to perform the operation on.
   */
  public function __construct(EmailInterface $email) {
    $this->email = $email;
  }

  /**
   * Get all operation links.
   *
   * @return array
   *   An array of operation links.
   */
  public function getAll() {
    return $this->operations;
  }

  /**
   * Add the "Confirm" link to the array of operations.
   */
  public function addConfirm() {
    $this->operations['confirm'] = [
      'title' => $this->t('Confirm'),
      'weight' => 5,
      'url' => Url::fromRoute(
        'multiple_email.confirm',
        ['multiple_email' => $this->email->id()],
      ),
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 'auto',
        ]),
      ],
    ];
  }

  /**
   * Add the "Set primary" link to the array of operations.
   */
  public function addSetPrimary() {
    $this->operations['set-primary'] = [
      'title' => $this->t('Set Primary'),
      'weight' => 10,
      'url' => Url::fromRoute(
        'multiple_email.set_primary',
        ['multiple_email' => $this->email->id()],
      ),
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 'auto',
        ]),
      ],
    ];
  }

  /**
   * Add the "Resend confirmation" link to the array of operations.
   */
  public function addResend() {
    $this->operations['resend'] = [
      'title' => $this->t('Resend confirmation'),
      'weight' => 15,
      'url' => Url::fromRoute(
        'multiple_email.resend',
        ['multiple_email' => $this->email->id()],
      ),
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 'auto',
        ]),
      ],
    ];
  }

  /**
   * Add the "Cancel confirmation" link to the array of operations.
   */
  public function addCancel() {
    $this->operations['cancel'] = [
      'title' => $this->t('Cancel Confirmation'),
      'weight' => 20,
      'url' => Url::fromRoute(
        'multiple_email.cancel_confirmation',
        ['multiple_email' => $this->email->id()],
      ),
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 'auto',
        ]),
      ],
    ];
  }

  /**
   * Add the "Remove" link to the array of operations.
   */
  public function addRemove() {
    $this->operations['remove'] = [
      'title' => $this->t('Remove'),
      'weight' => 25,
      'url' => Url::fromRoute(
        'multiple_email.remove',
        ['multiple_email' => $this->email->id()],
      ),
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 'auto',
        ]),
      ],
    ];
  }

}
