<?php

namespace Drupal\multiple_email;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an interface for email entity storage classes.
 */
interface EmailStorageInterface extends ContentEntityStorageInterface {

  /**
   * Load all email addresses for a user account.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to load from.
   *
   * @return \Drupal\multiple_email\EmailInterface[]
   *   An array of email entities.
   */
  public function loadByUser(AccountInterface $account);

  /**
   * Load an email entity by email address.
   *
   * @param string $email
   *   The email address of the email entity to load.
   *
   * @return \Drupal\multiple_email\EmailInterface
   *   The loaded email entity.
   */
  public function loadByEmail($email);

}
