<?php

namespace Drupal\Tests\multiple_email\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Url;

/**
 * Test user interactions with the module.
 *
 * @group multiple_email
 */
class ConfirmationTest extends FunctionalTestBase {
  use AssertMailTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->emailUser);
  }

  /**
   * Test that the confirmation email is sent when adding a new email address.
   */
  public function xtestConfirmationSent() {
    $this->drupalLogin($this->emailUser);
    $this->drupalGet('user/' . $this->emailUser->id() . '/edit/email-addresses');

    $new_email = $this->randomEmail();
    $edit = ['emails[add][email]' => $new_email];
    $this->submitForm($edit, 'Add email address');

    $subject = $this->t('Confirm your email address at @site', [
      '@site' => \Drupal::config('system.site')->get('name'),
    ]);

    $this->assertMail('to', $new_email);
    $this->assertMail('subject', $subject);
  }

  /**
   * Test the confirmation form.
   */
  public function testConfirmationForm() {
    $web_assert = $this->assertSession();
    $email_confirmer = \Drupal::service('multiple_email.confirmer');

    $this->drupalLogin($this->emailUser);

    $email = $this->createEmail($this->emailUser);
    $email_confirmer->confirm($email);

    $url = Url::fromRoute('multiple_email.confirm_form', [
      'user' => $this->emailUser->id(),
      'multiple_email' => $email->id(),
      'code' => 'invalid',
    ]);
    $this->drupalGet($url->toString());
    $this->submitForm([], 'Confirm');

    $message = $this->t('The confirmation code was incorrect');
    $web_assert->pageTextContains($message);

    $url = Url::fromRoute('multiple_email.confirm_form', [
      'user' => $this->emailUser->id(),
      'multiple_email' => $email->id(),
      'code' => $email->getConfirmationCode(),
    ]);

    $this->drupalGet($url->toString());
    $this->submitForm([], 'Confirm');

    $message = $this->t('The address @email has been confirmed.', [
      '@email' => $email->getEmail(),
    ]);
    $web_assert->pageTextContains($message);
  }

}
