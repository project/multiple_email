<?php

namespace Drupal\Tests\multiple_email\Functional;

use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test module being installed after users are already created.
 *
 * @group multiple_email
 */
class LateInstallTest extends BrowserTestBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * User with permission to use multiple emails.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $emailUser;

  /**
   * User with no additional permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $basicUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer users',
      'administer permissions',
      'administer modules',
      'administer site configuration',
    ]);

    $this->emailUser = $this->drupalCreateUser(['access content']);

    $this->basicUser = $this->drupalCreateUser();
  }

  /**
   * Test that the Multiple E-mails user can add a new e-mail address.
   */
  public function testLateInstall() {
    $web_assert = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    $url = Url::fromRoute('system.modules_list');
    $this->drupalGet($url->toString());
    $web_assert->pageTextContains('Multiple E-mail Addresses');
    $this->submitForm(['modules[multiple_email][enable]' => TRUE], $this->t('Install'));

    $link_text_parts = [
      $this->t('Administer'),
      $this->t('Configuration'),
      $this->t('People'),
      $this->t('Multiple Email'),
    ];
    $link_text = implode(' > ', $link_text_parts);

    $link = Link::createFromRoute($link_text, 'multiple_email.admin.settings');
    $install_message = $this->t('Multiple Email settings are available under @link', [
      '@link' => $link->toString(),
    ]);
    $web_assert->responseContains($install_message);

    $admin_roles = $this->adminUser->getRoles();
    $email_roles = $this->emailUser->getRoles();

    $url = Url::fromRoute('user.admin_permissions');
    $this->drupalGet($url->toString());
    $permissions = [
      $admin_roles[1] . '[administer multiple emails]' => TRUE,
      $email_roles[1] . '[use multiple emails]' => TRUE,
    ];
    $this->submitForm($permissions, $this->t('Save permissions'));

    $url = Url::fromRoute('multiple_email.admin.settings');
    $this->drupalGet($url->toString());
    $web_assert->statusCodeEquals(Response::HTTP_OK);

    $this->drupalLogin($this->emailUser);
    $this->drupalGet($url->toString());
    $web_assert->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $url = Url::fromRoute('multiple_email.manage', [
      'user' => $this->emailUser->id(),
    ]);
    $this->drupalGet($url->toString());
    $web_assert->statusCodeEquals(Response::HTTP_OK);
    $web_assert->pageTextContains($this->emailUser->getEmail());

    $this->drupalLogin($this->basicUser);
    $url = Url::fromRoute('multiple_email.admin.settings');
    $this->drupalGet($url->toString());
    $web_assert->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $url = Url::fromRoute('multiple_email.manage', [
      'user' => $this->basicUser->id(),
    ]);
    $this->drupalGet($url->toString());
    $web_assert->statusCodeEquals(Response::HTTP_FORBIDDEN);
  }

}
