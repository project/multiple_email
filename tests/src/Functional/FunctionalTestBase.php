<?php

namespace Drupal\Tests\multiple_email\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\multiple_email\Traits\MultipleEmailTestTrait;

/**
 * Common functionality for functional tests.
 */
abstract class FunctionalTestBase extends BrowserTestBase {
  use MultipleEmailTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'multiple_email',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createUsers();
  }

}
