<?php

namespace Drupal\Tests\multiple_email\Functional;

use Symfony\Component\HttpFoundation\Response;

/**
 * Test basic user interactions with the module.
 *
 * @group multiple_email
 */
class BasicUserTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->basicUser);

  }

  /**
   * Test that a basic user can't access the manage email addresses form.
   */
  public function testAccessDenied() {
    $web_assert = $this->assertSession();

    $this->drupalGet('user/' . $this->basicUser->id() . '/edit/email-addresses');
    $web_assert->statusCodeEquals(Response::HTTP_FORBIDDEN);
  }

  /**
   * Test that a basic user can edit their email address.
   */
  public function testMultipleEmailBasicUser() {
    $web_assert = $this->assertSession();

    $this->drupalGet('user/' . $this->basicUser->id() . '/edit');
    $web_assert->responseContains($this->basicUser->getEmail());

    $edit = [
      'mail' => $this->randomEmail(),
      'current_pass' => $this->basicUser->pass_raw,
    ];

    $this->submitForm($edit, 'Save');
    $web_assert->pageTextContains('The changes have been saved.');
    $web_assert->responseContains($edit['mail']);
  }

}
