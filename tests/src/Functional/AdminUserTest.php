<?php

namespace Drupal\Tests\multiple_email\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test admin user interactions with the module.
 *
 * @group multiple_email
 */
class AdminUserTest extends FunctionalTestBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test that the admin user can save the settings form.
   */
  public function testSettingsForm() {
    $url = Url::fromRoute('multiple_email.admin.settings');
    $this->drupalGet($url->toString());
    $this->assertSession()->pageTextContains('Multiple E-mails');
    $this->submitForm([], $this->t('Save configuration'));
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
  }

  /**
   * Test that an admin user can access other users email addresses.
   */
  public function testAccess() {
    $web_assert = $this->assertSession();

    $this->drupalGet('user/' . $this->emailUser->id() . '/edit/email-addresses');
    $web_assert->statusCodeEquals(Response::HTTP_OK);

    $this->drupalGet('user/' . $this->basicUser->id() . '/edit/email-addresses');
    $web_assert->statusCodeEquals(Response::HTTP_OK);
  }

  /**
   * Test that an admin user can add an email address to another users account.
   */
  public function testAdd() {
    $web_assert = $this->assertSession();

    $this->drupalGet('user/' . $this->emailUser->id() . '/edit/email-addresses');

    $new_email = $this->randomEmail();
    $edit = ['emails[add][email]' => $new_email];
    $this->submitForm($edit, 'Add email address');

    $success_message = $this->t('Email address @email added.', [
      '@email' => $new_email,
    ]);
    $web_assert->pageTextContains($success_message);
  }

}
