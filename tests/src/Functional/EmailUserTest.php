<?php

namespace Drupal\Tests\multiple_email\Functional;

use Drupal\Core\Test\AssertMailTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test user interactions with the module.
 *
 * @group multiple_email
 */
class EmailUserTest extends FunctionalTestBase {
  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->emailUser);
  }

  /**
   * Test that the user can access the manage email addresses form.
   */
  public function testAccess() {
    $web_assert = $this->assertSession();

    $this->drupalGet('user/' . $this->emailUser->id() . '/edit/email-addresses');
    $web_assert->statusCodeEquals(Response::HTTP_OK);
  }

}
