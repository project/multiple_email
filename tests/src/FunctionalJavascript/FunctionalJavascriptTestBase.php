<?php

namespace Drupal\Tests\multiple_email\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\multiple_email\Traits\MultipleEmailTestTrait;

/**
 * Common functionality for functional javascript tests.
 */
abstract class FunctionalJavascriptTestBase extends WebDriverTestBase {
  use MultipleEmailTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'multiple_email',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createUsers();
    $this->drupalLogin($this->emailUser);
  }

}
