<?php

namespace Drupal\Tests\multiple_email\FunctionalJavascript;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\multiple_email\EmailInterface;

/**
 * Test the functionality of the email addreses management form.
 *
 * @group multiple_email
 */
class ManageFormTest extends FunctionalJavascriptTestBase {
  use StringTranslationTrait;

  /**
   * Test adding a new email address.
   */
  public function testAddEmail() {
    $web_assert = $this->assertSession();

    $url = Url::fromRoute('multiple_email.manage', [
      'user' => $this->emailUser->id(),
    ]);
    $this->drupalGet($url->toString());

    $new_email = $this->randomEmail();
    $edit = ['emails[add][email]' => $new_email];
    $this->submitForm($edit, 'Add email address');

    $success_message = $this->t('Email address @email added.', [
      '@email' => $new_email,
    ]);
    $web_assert->pageTextContains($success_message);

    /** @var \Drupal\multiple_email\EmailStorageInterface $email_storage */
    $email_storage = \Drupal::entityTypeManager()->getStorage('multiple_email');
    $email = $email_storage->loadByEmail($new_email);
    $this->assertInstanceOf(EmailInterface::class, $email);
  }

  /**
   * Test that the correct default status shows for email addresses.
   */
  public function testDefaultStatus() {
    /** @var \Drupal\multiple_email\EmailStorageInterface $email_storage */
    $email_storage = \Drupal::entityTypeManager()->getStorage('multiple_email');
    $primary_email = $email_storage->loadByEmail($this->emailUser->getEmail());
    $additional_email = $this->createEmail($this->emailUser);

    $url = Url::fromRoute('multiple_email.manage', [
      'user' => $this->emailUser->id(),
    ]);
    $this->drupalGet($url->toString());

    $row = $this->xpath('//tr[@data-drupal-selector=:selector]', [
      ':selector' => 'edit-emails-' . $primary_email->id(),
    ]);
    $row = reset($row);

    $primary_text = $this->t('Primary');
    $status = $row->find('css', 'td:nth-child(2)');
    $this->assertEquals($primary_text, $status->getText());

    $row = $this->xpath('//tr[@data-drupal-selector=:selector]', [
      ':selector' => 'edit-emails-' . $additional_email->id(),
    ]);
    $row = reset($row);

    $pending_text = $this->t('Confirmation pending');
    $status = $row->find('css', 'td:nth-child(2)');
    $this->assertEquals($pending_text, $status->getText());
  }

  /**
   * Test cancelling a confirmation.
   */
  public function testCancel() {
    $web_assert = $this->assertSession();
    $email = $this->createEmail($this->emailUser);

    $url = Url::fromRoute('multiple_email.manage', [
      'user' => $this->emailUser->id(),
    ]);
    $this->drupalGet($url->toString());

    $page = $this->getSession()->getPage();
    $row = $this->xpath('//tr[@data-drupal-selector=:selector]', [
      ':selector' => 'edit-emails-' . $email->id(),
    ]);
    $row = reset($row);

    $dropbutton = $row->find('css', '.dropbutton-toggle button');
    $dropbutton->press();

    $row->clickLink('Cancel Confirmation');
    $web_assert->waitForElementVisible('css', '.ui-dialog');

    $confirm_button = $page->find('css', '.ui-dialog .ui-dialog-buttonpane .form-actions .button--primary');
    $this->assertNotEmpty($confirm_button);
    $confirm_button->press();
    $web_assert->assertWaitOnAjaxRequest();

    $cancelled_text = $this->t('Confirmation cancelled');
    $status = $row->find('css', 'td:nth-child(2)');
    $this->assertEquals($cancelled_text, $status->getText());
  }

  /**
   * Test removing an email address.
   */
  public function testRemove() {
    $web_assert = $this->assertSession();
    $email = $this->createEmail($this->emailUser);

    $url = Url::fromRoute('multiple_email.manage', [
      'user' => $this->emailUser->id(),
    ]);
    $this->drupalGet($url->toString());

    $page = $this->getSession()->getPage();
    $row = $this->xpath('//tr[@data-drupal-selector=:selector]', [
      ':selector' => 'edit-emails-' . $email->id(),
    ]);
    $row = reset($row);

    $dropbutton = $row->find('css', '.dropbutton-toggle button');
    $dropbutton->press();

    $row->clickLink('Remove');
    $web_assert->waitForElementVisible('css', '.ui-dialog');

    $confirm_button = $page->find('css', '.ui-dialog .ui-dialog-buttonpane .form-actions .button--primary');
    $this->assertNotEmpty($confirm_button);
    $confirm_button->press();
    $web_assert->assertWaitOnAjaxRequest();

    $web_assert->elementNotExists('xpath', $row->getXpath());
  }

}
