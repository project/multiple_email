<?php

namespace Drupal\Tests\multiple_email\Traits;

use Drupal\Tests\RandomGeneratorTrait;
use Drupal\user\UserInterface;

/**
 * Common functionality for Multiple Email tests.
 */
trait MultipleEmailTestTrait {
  use RandomGeneratorTrait;

  /**
   * User with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * User with permission to use multiple emails.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $emailUser;

  /**
   * User with no additional permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $basicUser;

  /**
   * Generates a random email address.
   *
   * @return string
   *   The newly created email.
   */
  public function randomEmail() {
    return $this->randomMachineName() . '@example.com';
  }

  /**
   * Create test users with varying permissions.
   */
  protected function createUsers() {
    $this->adminUser = $this->drupalCreateUser([
      'administer users',
      'administer multiple emails',
    ]);

    $this->emailUser = $this->drupalCreateUser([
      'use multiple emails',
    ]);

    $this->basicUser = $this->drupalCreateUser();
  }

  /**
   * Create an email address for a user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to create the email for.
   *
   * @return \Drupal\multiple_email\EmailInterface
   *   The newly created email.
   */
  protected function createEmail(UserInterface $user) {
    /** @var \Drupal\multiple_email\EmailStorageInterface $email_storage */
    $email_storage = \Drupal::entityTypeManager()->getStorage('multiple_email');

    /** @var \Drupal\multiple_email\EmailInterface $email */
    $email = $email_storage->create([
      'email' => $this->randomEmail(),
    ]);
    $email->setOwner($user);
    $email->save();

    return $email;
  }

}
